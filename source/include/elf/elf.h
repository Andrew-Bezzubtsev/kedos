#ifndef KEDOS_ELF_H
#define KEDOS_ELF_H

typedef uint16_t elf_half;
typedef uint32_t elf_off;
typedef uint32_t elf_addr;
typedef uint32_t elf_word;
typedef int32_t  elf_sword;

#define ELF_N_IDENT 16

struct elf_ehdr {
	uint8_t		e_ident[ELF_NIDENT];
	elf_half	e_type;
	elf_half	e_machine;
	elf_word	e_version;
	elf_addr	e_entry;
	elf_off 	e_phoff;
	elf_off         e_shoff;
	elf_word	e_flags;
	elf_half	e_ehsize;
	elf_half        e_phentsize;
	elf_half	e_phnum;
	elf_half	e_shentsize;
	elf_half	e_shnum;
	elf_half	e_shstrndx;
} Elf32_Ehdr;

enum elf_ident {
	ELF_IDENT_MAG0       = 0,
	ELF_IDENT_MAG1       = 1,
	ELF_IDENT_MAG2       = 2,
	ELF_IDENT_MAG3       = 3, 
	ELF_IDENT_CLASS	     = 4,
	ELF_IDENT_DATA       = 5,
	ELF_IDENT_VERSION    = 6,
	ELF_IDENT_OSABI	     = 7,
	ELF_IDENT_ABIVERSION = 8,
	ELF_IDENT_PAD	     = 9 
};

#endif /* KEDOS_ELF_H */
